<?php
get_header(); ?>

            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <article <?php post_class( 'article' ); ?> id="post-<?php the_ID(); ?>">
                        <header class="entry-header entry-header h2">
                            <?php edit_post_link( __( 'Edit Post', 'pg_blog' ) ); ?>
                            <a href="<?php echo esc_url( the_permalink() ); ?>"><h2><?php the_title(); ?></h2></a>
                        </header>
                        <div class="entry-author">
                            <?php _e( 'Posted on', 'pg_blog' ); ?>
                            <span><?php echo get_the_time( get_option( 'date_format' ) ) ?></span>
                            <span> <?php _e( 'by', 'pg_blog' ); ?> </span>
                            <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><span class="entry-author"><?php the_author_meta( 'nickname' ); ?></span></a>
                        </div>
                        <div class="entry-hr"></div>
                        <a href="<?php echo esc_url( get_permalink() ); ?>"><?php
                                if ( has_post_thumbnail() ) {
                                    the_post_thumbnail( 'pgblog-index' );
                                }
                             ?></a>
                        <div class="entry-content">
                            <div class="entry-excerpt">
                                <?php the_excerpt( ); ?>
                            </div>
                        </div>
                        <nav class="article-read-more">
                            <a href="<?php echo esc_url( post_permalink() ); ?>" class="big-button article-read-more-link left"><?php _e( 'Read More', 'pg_blog' ); ?></a>
                        </nav>
                        <div class="float-cleaner"></div>
                        <footer class="entry-footer">
</footer>
                    </article>
                <?php endwhile; ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.', 'pg_blog' ); ?></p>
            <?php endif; ?>
            <div class="index-navigation">
                <?php next_posts_link( 'Older Posts' ); ?>
                <?php previous_posts_link( __( 'Newer Posts', 'pg_blog' ) ); ?>
            </div>            

<?php get_footer(); ?>