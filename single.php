<?php
get_header( 'master-page-single' ); ?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <article <?php post_class( 'article' ); ?> id="post-<?php the_ID(); ?>">
            <?php edit_post_link( __( 'Edit Post', 'pg_blog' ) ); ?>
            <div class="entry-meta" id="entry-meta"></div>
            <header class="entry-header" id="entry-header">
                <?php if ( has_excerpt() ) : ?>
                    <em id="single-excerpt"><?php echo get_the_excerpt(); ?></em>
                <?php endif; ?>
            </header>
            <div class="entry-content" id="entry-content">
                <?php the_content(); ?>
            </div>
            <div class="social-sharing" id="social-sharing">
                <h2 class="share-title"><?php _e( 'Share this Post', 'pg_blog' ); ?></h2>
                <ul class="social-icons social-icons-lg social-icons-colored social-icons-round">
                    <li>
                        <a onclick="window.open('http://www.facebook.com/sharer.php?u=<?php echo esc_url( the_permalink() ); ?>','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)" target="_blank" title="Share on Facebook" class="bg-facebook"><i class="fa fa-facebook-f"></i></a>
                    </li>
                    <li>
                        <a href="https://twitter.com/share?url=<?php echo esc_url( the_permalink() ); ?>;text=<?php the_title(); ?> @pinegrow" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank" title="Share on Twitter" class="bg-twitter"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li>
                        <a href="https://plus.google.com/share?url=<?php echo esc_url( the_permalink() ); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank" title="Share on Google+" class="bg-google"><i class="fa fa-google-plus"></i></a>
                    </li>
                </ul>
            </div>
            <div class="comments" id="comments">
                <?php comments_template( 'comments.php' ); ?>
            </div>
            <nav class="post-navigation" id="bottom-nav">
                <div class="left">
                    <?php previous_post_link( '%link', __( 'PREV POST', 'pg_blog' ) ); ?>
                </div>
                <div class="right">
                    <?php next_post_link( '%link', __( 'NEXT POST', 'pg_blog' ) ); ?>
                </div>
                <div class="float-cleaner"></div>
            </nav>
        </article>
    <?php endwhile; ?>
<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.', 'pg_blog' ); ?></p>
<?php endif; ?>

<?php get_footer( 'master-page-single' ); ?>