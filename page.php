<?php
get_header( 'master-page-single' ); ?>

            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <article <?php post_class( 'article' ); ?> id="post-<?php the_ID(); ?>">
                        <?php edit_post_link( __( 'Edit Page', 'pg_blog' ) ); ?>
                        <header class="entry-header" id="entry-header">
                            <?php if ( has_excerpt() ) : ?>
                                <em><?php echo get_the_excerpt(); ?></em>
                            <?php endif; ?>
                        </header>
                        <div class="entry-content" id="entry-content">
                            <?php the_content(); ?>
                        </div>
                        <div class="entry-meta"></div>
                        <footer class="entry-footer">
</footer>
                    </article>
                <?php endwhile; ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.', 'pg_blog' ); ?></p>
            <?php endif; ?>

<?php get_footer( 'master-page-single' ); ?>