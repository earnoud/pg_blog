<?php
get_header(); ?>

            <?php
                $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
            ?>
            <article class="article" id="author-bio">
                <div>
                    <?php if ( ! function_exists( 'get_field' ) ) : ?>
                        <p><?php _e( 'Please, Install', 'pg_blog' ); ?> <a href="https://www.advancedcustomfields.com/pro/"><?php _e( 'ACF PRO', 'pg_blog' ); ?></a> <?php _e( 'or User', 'pg_blog' ); ?> <a href="https://wordpress.org/plugins/user-photo/"><?php _e( 'User Photo', 'pg_blog' ); ?></a></p>
                    <?php endif; ?>
                    <?php if ( function_exists( 'get_field' ) ) : ?>
                        <div>
                            <?php
                                $user_picture = get_field('user_picture', 'user_' . $curauth->ID);
                            ?>
                            <?php if ( $user_picture ) : ?>
                                <img src="<?php echo $user_picture ?>" />
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                    <?php if ( function_exists( 'userphoto_the_author_photo' ) ) : ?>
                        <div>
                            <?php userphoto_the_author_photo() ?>
                        </div>
                    <?php endif; ?>
                    <p><?php the_author_meta( 'description' ); ?></p>
                </div>
            </article>
            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class( 'article' ); ?>>
                        <header class="entry-header entry-header h2">
                            <?php edit_post_link( __( 'Edit Post', 'pg_blog' ) ); ?>
                            <a href="<?php echo esc_url( the_permalink() ); ?>"><h2><?php the_title(); ?></h2></a>
                        </header>
                        <div class="entry-author">
                            <?php _e( 'Posted on', 'pg_blog' ); ?>
                            <span><?php echo get_the_time( get_option( 'date_format' ) ) ?></span>
                            <span> <?php _e( 'by', 'pg_blog' ); ?> </span>
                            <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><span class="entry-author"><?php the_author_meta( 'nickname' ); ?></span></a>
                        </div>
                        <div class="entry-hr"></div>
                        <a href="<?php echo esc_url( get_permalink() ); ?>"><?php
                                if ( has_post_thumbnail() ) {
                                    the_post_thumbnail( 'pgblog-index' );
                                }
                             ?></a>
                        <div class="entry-content">
                            <div class="entry-excerpt">
                                <?php the_excerpt( ); ?>
                            </div>
                        </div>
                        <nav class="article-read-more">
                            <a href="<?php echo esc_url( post_permalink() ); ?>" class="big-button article-read-more-link left"><?php _e( 'Read More', 'pg_blog' ); ?></a>
                        </nav>
                        <div class="float-cleaner"></div>
                        <footer class="entry-footer">
</footer>
                    </article>
                <?php endwhile; ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.', 'pg_blog' ); ?></p>
            <?php endif; ?>
            <div class="index-navigation">
                <?php next_posts_link( 'Older Posts' ); ?>
                <?php previous_posts_link( __( 'Newer Posts', 'pg_blog' ) ); ?>
            </div>            

<?php get_footer(); ?>