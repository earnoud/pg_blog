<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php wp_head(); ?>
    </head>
    <body class="<?php echo implode(' ', get_body_class()); ?>">
        <?php
            $image_attributes = (is_singular() || in_the_loop()) ? wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'pgblog-header' ) : null;
            if( !$image_attributes  && ( $header_image = get_header_image() ) ) $image_attributes = array( $header_image );
        ?>
        <header id="masthead" class="site-header" style="<?php if($image_attributes) echo 'background-image:url(\''.$image_attributes[0].'\')' ?>">
            <div class="dimmer">
                <nav>
                    <?php if ( has_custom_logo() ) : ?>
                        <?php pg_starter_the_custom_logo() ?>
                    <?php endif; ?>
                </nav>
                <div class="site-header-title">
                    <h1><?php the_title(); ?></h1>
                    <div class="site-header-meta" id="header-meta">
                        <?php the_category(); ?>
                        <span class="thedate"><?php echo get_the_time( get_option( 'date_format' ) ) ?></span>
                        <br />
                    </div>
                </div>
                <br />
                <?php
                    /* Grabbing Post Author */
                    $author_id = get_queried_object()->post_author;
                ?>
                <a href="<?php echo esc_url( get_author_posts_url( $author_id ) ); ?>" class="small-button site-header-link"><span><?php the_author_meta( 'display_name', $author_id ); ?></span></a>
            </div>
        </header>
        <main id="main" class="site-content site-inner">