
        </main>
        <footer class="site-footer footer">
            <h2 class="footer-headings"><?php _e( 'Build websites faster with', 'pg_blog' ); ?> <a href="http://pinegrow.com"><?php _e( 'Pinegrow Web Editor', 'pg_blog' ); ?></a></h2>
            <p class="footer-paragraph"><?php _e( 'a desktop app that lets you build responsive websites faster with live multi-page editing, CSS styling and smart components for Bootstrap, Foundation, AngularJS and WordPress.', 'pg_blog' ); ?></p>
        </footer>
        <?php wp_footer(); ?>
    </body>
</html>
